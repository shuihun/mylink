<?php

/**
 * 缓存操作类
 * author: 爱是西瓜
 * DateTime: 2018/5/01
 */

class Cache{

    /*********************客服相关**************************/

    /**
     * 获取客服列表
     * @return mixed
     */
    public static function kfList(){
        $kfList = $GLOBALS['kfList'];
        return $kfList?$kfList:[];
    }

    /**
     * 根据用户编号获取用户信息
     * @param $user_id
     * @return mixed
     */
    public static function getUidKf($user_id){
        return $GLOBALS['kfList'][$user_id];
    }

    /**
     * 根据用户编号设置用户信息
     * @param $user_id
     * @param $data
     * @return mixed
     */
    public static function setUidKf($user_id,$data){

        if(empty($user_id)) return false;
        if(empty($data)){
            unset($GLOBALS['kfList'][$user_id]);
            return false;
        }

        return $GLOBALS['kfList'][$user_id] = $data;
    }



    /*************************链接信息*****************************/

    /**
     * 获取链接用户列表
     * @return mixed
     */
    public static function getCliList(){
        $cliList = $GLOBALS['fd'];
        return $cliList?$cliList:[];
    }

    /**
     * 根据用户连接号获取用户信息
     * @param $fd
     * @return mixed
     */
    public static function getCliUser($fd){
        return $GLOBALS['fd'][$fd];
    }


    /**
     * 根据用户连接号设置用户信息
     * @param $fd
     * @param $data
     * @return mixed
     */
    public static function setCliUser($fd,$data){

        if(empty($fd)) return false;
        if(empty($data)){

            unset($GLOBALS['fd'][$fd]);

            Db::update(['end_time'=>time()],'ws_service_log',"WHERE client_id='{$fd}'");

            return false;
        }

        return $GLOBALS['fd'][$fd] = $data;
    }










}
