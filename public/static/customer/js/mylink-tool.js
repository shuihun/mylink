var myLink = function(){

    //var isOpen = 0;
    var baseConfig = {
        domain: 'http://mylink.mbku.net',
        title: 'mylink客服',
        group: 0
    };

    var self = this;

    self.init = function(config){

        baseConfig = self.extend(baseConfig, config || {});
        console.log(baseConfig);
        if( '' == baseConfig.mytoken || 0 == baseConfig.group){
            alert("参数缺失");
            return false;
        }

        var url = baseConfig.domain + '/index/index/chat' + '?group=' + baseConfig.group + '&mytoken=' + baseConfig.mytoken;

        if(self.isMobile()){
            window.location.href = url;
        }else{
            self.openWindow(url, baseConfig.title);
        }
    };

    // 合并配置项
    self.extend = function(target, source){
        for (var obj in source) {
            target[obj] = source[obj];
        }
        return target;
    };

    // 打开窗口
    self.openWindow = function(url, name){
        layer.open({
            type: 2,
            id: 'mylink', // 允许打开一次
            title: name,
            shade: 0,
            area: ['800px', '640px'],
            content: url
        });
    };

    // 是否是移动端
    self.isMobile = function(){
        if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ){
            return true;
        } else {
            return false;
        }
    };
};
