# MyLink客服系统

#### 项目介绍
采用 thinkphp5 + swoole 编写，性能强悍，您只需要一段js便可以使您的网站拥有客服功能。

#### 软件架构
程序基于thinkphp5开发，通讯模块基于swoole开发，因此服务器需要基于linux，PHP建议7.0以上（性能更好未来趋势~）


#### 安装教程

1. 将代码上传到站点空间，根目录指向public(thinkphp5默认public根目录)
2. 初次访问，进入安装环节，校验目录权限与模块开启等
3. 配置数据库信息与后台基本信息
4. 安装完成进入后台配置

#### 使用说明

1. 首先，拿到下载到的文件，先指定域名到public文件。导入数据库，并配置好连接。
2. 通过命令，启动并后台运行swoole服务 （web/web.sh）
3. 引入指定的js，并且配置相关的参数。


#### 客服功能

1. 支持客服分组，多客服服务，让您的服务更有条理。
2. 支持客服转接，让会员接受最专业的服务。
3. 智能分配客户流量，让服务更加高效。
4. 问候语设置，服务更加亲切。
5. 历史聊天记录查看，方便监管和总结。
6. 支持数据趋势统计，随时掌握服务情况，做出最优的调整。


***
## mylink客服系统交流群
QQ群: 452919578

***
## 打赏作者  
支付宝
![支付宝](http://mylink.mbku.net/static/home/images/alipay_200.jpg)  
微信
![微信](http://mylink.mbku.net/static/home/images/wxpay_200.jpg) 


***
## 官网:
[mylink官网  http://mylink.mbku.net](http://mylink.mbku.net)  
[mylink文档  http://doc.mbku.net/mylink](http://doc.mbku.net/mylink)
